package tracks.multiPlayer.CE811_to17758;

import java.util.Random;

public class Game {

    public static int pId;              // ID of the player.
    public static int oId;              // ID of the opponent.
    public static int players = 2;      // Number of players in the game.
    public static int maxActions = 6;   // Max number of actions available in the game.
    public static Random random;        // Random number generator used during the course of the game.
}