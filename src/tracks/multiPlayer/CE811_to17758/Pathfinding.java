package tracks.multiPlayer.CE811_to17758;

import core.game.StateObservationMulti;
import tools.Vector2d;
import java.util.PriorityQueue;

public class Pathfinding {
    public static int findPathLength(Vector2d avatarPosition, Vector2d targetPosition, StateObservationMulti state){
        Grid grid = new Grid(state);

        int blockSize = state.getBlockSize();

        //change position so that it is in grid size
        int avatarPositionX = (int)avatarPosition.x / blockSize;
        int avatarPositionY = (int)avatarPosition.y / blockSize;
        int targetPositionX = (int)targetPosition.x / blockSize;
        int targetPositionY = (int)targetPosition.y / blockSize;
        //objects can sometimes leave the grid without getting removed, if that happens we do not initiate AStar, since that results in an infinite loop
        if (avatarPositionX >= grid.gridLengthX || avatarPositionY >= grid.gridLengthY || targetPositionX >= grid.gridLengthX || targetPositionY >= grid.gridLengthY){
            return 0;
        }
        Vector2d avatarPositionGrid = new Vector2d(avatarPositionX, avatarPositionY);
        Vector2d targetPositionGrid = new Vector2d(targetPositionX, targetPositionY);

        PriorityQueue<GridNode> openNodes = new PriorityQueue<>();
        PriorityQueue<GridNode> closedNodes = new PriorityQueue<>();

        GridNode startNode = new GridNode(avatarPositionGrid);
        GridNode targetNode = new GridNode(targetPositionGrid);
        openNodes.add(startNode);

        while(!openNodes.isEmpty()){
            // Old search version, priority queue performs much better, almost twice as fast
            //GridNode lowestCostNode = openNodes.get(0);
            //for (int i = 1; i < openNodes.size(); i++){
            //    GridNode compareNode = openNodes.get(i);
            //    if ( compareNode.getfCost() < lowestCostNode.getfCost() || (compareNode.getfCost() == lowestCostNode.getfCost() && compareNode.hCost < lowestCostNode.hCost)){
            //        lowestCostNode = compareNode;
            //    }
            //}

            //Now using priority queue
            GridNode lowestCostNode = openNodes.peek();

            openNodes.remove(lowestCostNode);
            closedNodes.add(lowestCostNode);

            if (lowestCostNode.position.equals(targetPositionGrid)){
                return calculatePathLength(lowestCostNode);
            }

            for (GridNode neighbour : grid.getNeighbours(lowestCostNode)) {
//                if (!neighbour.isTraversable || closedNodes.contains(neighbour)){
//                    //if (!neighbour.isTraversable){
//                    //    System.out.println("You shall not pass!");
//                    //}
//                    continue;
//                }
                int newDistance = lowestCostNode.gCost + calculateDistance(lowestCostNode, neighbour);
                if (neighbour.gCost > newDistance || !openNodes.contains(neighbour)){
                    neighbour.gCost = newDistance;
                    neighbour.hCost = calculateDistance(neighbour, targetNode);
                    neighbour.parent = lowestCostNode;
                    if (!openNodes.contains(neighbour)){
                        openNodes.add(neighbour);
                    }
                }
            }
        }

        //System.out.println("Target unreachable");
        return 0;
    }

    private static int calculatePathLength(GridNode endNode){
        int pathLength = 0;
        GridNode currentNode = endNode;
        while(currentNode.parent != null){
            pathLength++;
            currentNode = currentNode.parent;
        }
        return pathLength;
    }

    //assuming it is a 4 move grid
    private static int calculateDistance(GridNode node1, GridNode node2){
        int distanceX = Math.abs(node1.xPosition - node2.xPosition);
        int distanceY = Math.abs(node1.yPosition - node2.yPosition);

        return distanceX + distanceY;
    }
}
