package tracks.multiPlayer.CE811_to17758;

import core.game.StateObservationMulti;
import ontology.Types;
import tools.ElapsedCpuTimer;
import tools.Utils;

import java.util.ArrayList;

public class MonteCarloTreeSearch {

    private Kbe kbe;

    private static final int MAX_ROLL_OUT_DEPTH = 10;
    private static final int TIME_REMAINING_LIMIT = 5;
    private static final int BFS_BRANCH_ITERATIONS = 1;
    private static final double GAMMA = 0.9;
    private static final int GAMMA_DECAY = 2;
    private static final double HUGE_POSITIVE = 10000000.0;
    private static final double HUGE_NEGATIVE = -10000000.0;
    private static final double DEATH_NEGATIVE = -1000000000.0;
    private static final double EXPLORATION_CONSTANT = Math.sqrt(2); // Higher value -> More exploration.

    public MonteCarloTreeSearch(StateObservationMulti s) {
        this.kbe = new Kbe(s);
    }

    // MCTS core functions.

    public Types.ACTIONS search(StateObservationMulti s, ElapsedCpuTimer e) {

        double tAvg = 0;                        // Average time taken.
        double tAcm = 0;                        // Accumulated time taken.
        long tRem = e.remainingTimeMillis();    // Time remaining.
        int i = 0;                              // Iterations completed.

        double delta;

        Node n;
        Node root = new Node(null);

        StateObservationMulti si;
        ElapsedCpuTimer elps;

        kbe.incrementWeights();
        breadthFirst(root, s);

        while (timeRemaining(tRem, tAvg)) {
            si = s.copy();
            elps = new ElapsedCpuTimer();

            n = treePolicy(root, si);
            delta = rollout(n, si);
            backpropagation(n, delta);

            tAcm += (elps.elapsedMillis());
            tAvg = tAcm / ++i;
            tRem = e.remainingTimeMillis();
        }

        //System.out.println(i);
        return selectAction(root);
    }

    private Node treePolicy(Node root, StateObservationMulti s) {

        Node n = root;

        while (!stop(s, n.depth)) {
            if (!n.expanded(s)) {
                return expand(n, s);
            } else {
                n = uct(n, s);
            }
        }

        return n;
    }

//    private double rollout(Node n, StateObservationMulti s) {
//
//        double value = 0;   // Value of the state, calculated during roll out.
//
//        double valuePost;   // Value of the state, before advance.
//        double valuePrior;  // Value of the state, after advance.
//
//        int step = 1;       // Step # of the roll out.
//        int d = n.depth;    // Starting depth of the roll out.
//
//        Types.ACTIONS[] actions = new Types.ACTIONS[Game.players];
//
//        while (!stop(s, d)) {
//            valuePrior = value(s);
//            kbe.observe(s); // Categorise and store IDs of all observed sprites.
//            kbe.updateSpriteDistancesPrior(s);  // Compute and store current distances to all sprites.
//
//            // Advance to the next game state, selecting random action for both players.
//            actions[Game.pId] = randomAction(s, Game.pId);
//            actions[Game.oId] = randomAction(s, Game.oId);
//            s.advance(actions);
//
//            valuePost = value(s);
//            kbe.observe(s); // Observe new sprites.
//            kbe.updateSpriteDistances(s);  // Compute and store new distances.
//
//            if (valuePrior != valuePost) {  // Score change, assume collision responsible.
//                if (!s.getEventsHistory().isEmpty()) {
//                    kbe.updateWeight(s.getEventsHistory().last().passiveTypeId, valuePost - valuePrior);
//                }
//            }
//
//            value += (kbe.evaluate() * Math.pow(GAMMA, step)); // Exponential decay of KBE influence.
//
//            step += GAMMA_DECAY;
//
//            d++;
//        }
//
//        return (value + value(s));
//    }

    private double rollout(Node n, StateObservationMulti s) {

        double value = 0;   // Value of the state, calculated during roll out.

        double valuePost;   // Value of the state, before advance.
        double valuePrior;  // Value of the state, after advance.

        // int step = 1;       // Step # of the roll out.
        int d = n.depth;    // Starting depth of the roll out.

        Types.ACTIONS[] actions = new Types.ACTIONS[Game.players];

        while (!stop(s, d)) {
            valuePrior = value(s);
            //kbe.observe(s); // Categorise and store IDs of all observed sprites.
            //kbe.updateSpriteDistancesPrior(s);  // Compute and store current distances to all sprites.

            // Advance to the next game state, selecting random action for both players.
            actions[Game.pId] = randomAction(s, Game.pId);
            actions[Game.oId] = randomAction(s, Game.oId);
            s.advance(actions);

            valuePost = value(s);
            //kbe.observe(s); // Observe new sprites.
            //kbe.updateSpriteDistances(s);  // Compute and store new distances.

            if (valuePrior != valuePost) {  // Score change, assume collision responsible.
                if (!s.getEventsHistory().isEmpty()) {
                    kbe.updateWeight(s.getEventsHistory().last().passiveTypeId, valuePost - valuePrior);
                }
            }

            // value += (kbe.evaluate() * Math.pow(GAMMA, step)); // Exponential decay of KBE influence.

            // step += GAMMA_DECAY;

            d++;
        }

        return (value + value(s));
    }

    private void backpropagation(Node n, double delta) {
        while (n != null) {
            n.visits++;
            n.value += delta;
            n.updateBounds(delta);
            n = n.parent;
        }
    }

    private Node uct(Node n, StateObservationMulti s) {

        double uct;
        double max = -Double.MAX_VALUE;

        int selectedIdx = 0;
        Node selectedNode = null;

        for (int i = 0; i < n.children.length; i++) {
            if (n.children[i] != null) {

                uct = Utils.normalise((n.children[i].value / n.children[i].visits), n.lowerBound, n.upperBound)
                        + (EXPLORATION_CONSTANT * (Math.sqrt(Math.log(n.visits) / n.children[i].visits)));

                if (uct > max) {
                    selectedNode = n.children[i];
                    selectedIdx = i;
                    max = uct;
                }
            }
        }

        Types.ACTIONS[] actions = new Types.ACTIONS[Game.players];
        actions[Game.pId] = n.branches[selectedIdx];
        actions[Game.oId] = randomAction(s, Game.oId);

        s.advance(actions);

        return selectedNode;
    }

    private Node expand(Node n, StateObservationMulti s) {

        double x;
        double max = 0;
        int playerActionIdx = 0;
        for (int i = 0; i < actions(s, Game.pId).length; i++) {
            x = Game.random.nextDouble();
            if (x > max && n.children[i] == null) {
                playerActionIdx = i;
                max = x;
            }
        }

        Types.ACTIONS[] actions = new Types.ACTIONS[Game.players];
        actions[Game.pId] = action(s, Game.pId, playerActionIdx);
        actions[Game.oId] = randomAction(s, Game.oId);

        s.advance(actions);

        Node child = new Node(n);

        n.branches[playerActionIdx] = actions[Game.pId];
        n.children[playerActionIdx] = child;

        return child;
    }

    private Node expand(Node n, StateObservationMulti s, int playerActionIdx) {

        Types.ACTIONS[] actions = new Types.ACTIONS[Game.players];
        actions[Game.pId] = action(s, Game.pId, playerActionIdx);
        actions[Game.oId] = randomAction(s, Game.oId);

        s.advance(actions);

        Node child = new Node(n);

        n.branches[playerActionIdx] = actions[Game.pId];
        n.children[playerActionIdx] = child;

        return child;
    }

    // Breadth-first search.

//    private void breadthFirst(Node root, StateObservationMulti rootState) {
//
//        Node rootChild;
//        double value;
//        int actionIndex = 0;
//
//        while (!root.expanded(rootState)) { // Expand all root nodes.
//            StateObservationMulti rootBranchState = rootState.copy();
//
//            rootChild = expand(root, rootBranchState, actionIndex);
//            value = value(rootBranchState);
//
//            for (int i = 0; i < BFS_BRANCH_ITERATIONS; i++) { // More iterations to account for non-determinism.
//                StateObservationMulti rootBranchStateIter = rootState.copy();
//                Types.ACTIONS[] actions = new Types.ACTIONS[Game.players];
//                actions[Game.pId] = action(rootBranchStateIter, Game.pId, actionIndex);
//                actions[Game.oId] = randomAction(rootBranchStateIter, Game.oId);
//
//                rootBranchStateIter.advance(actions);
//                value += value(rootBranchStateIter);   // Choose from different evaluation functions.
//                // scoreTotal += valueBreadthFirst(copiedState);
//            }
//
//            backpropagation(rootChild, value / (BFS_BRANCH_ITERATIONS + 1));
//            actionIndex++;
//        }
//    }

    private void breadthFirst(Node root, StateObservationMulti rootState) {

        Node rootChild;
        double value;
        int actionIndex = 0;

        kbe.observe(rootState);
        kbe.updateSpriteDistancesPrior(rootState);

        while (!root.expanded(rootState)) { // Expand all root nodes.

            StateObservationMulti rootBranchState = rootState.copy();
            rootChild = expand(root, rootBranchState, actionIndex);

            kbe.observe(rootBranchState);
            kbe.updateSpriteDistances(rootBranchState);

            value = value(rootBranchState);
            value += kbe.evaluate();

            for (int i = 0; i < BFS_BRANCH_ITERATIONS; i++) { // More iterations to account for non-determinism.
                StateObservationMulti rootBranchStateIter = rootState.copy();
                Types.ACTIONS[] actions = new Types.ACTIONS[Game.players];
                actions[Game.pId] = action(rootBranchStateIter, Game.pId, actionIndex);
                actions[Game.oId] = randomAction(rootBranchStateIter, Game.oId);

                kbe.observe(rootBranchState);
                kbe.updateSpriteDistances(rootBranchState);

                rootBranchStateIter.advance(actions);
                value += value(rootBranchStateIter);   // Choose from different evaluation functions.
                value += kbe.evaluate();
                // scoreTotal += valueBreadthFirst(copiedState);
            }

            backpropagation(rootChild, value / (BFS_BRANCH_ITERATIONS + 1));
            actionIndex++;
        }
    }

    private double valueBreadthFirst(StateObservationMulti state) {

        double score = 0;
        boolean gameOver = state.isGameOver();

        Types.WINNER win = state.getMultiGameWinner()[Game.pId];

        if (gameOver && win == Types.WINNER.PLAYER_LOSES) {
            score += DEATH_NEGATIVE;
        }

        return score;
    }

    private double valueBreadthFirstDeath(StateObservationMulti state) {

        double score = 0;

        if (!state.isAvatarAlive(Game.pId)) {
            score += DEATH_NEGATIVE;
        }

        return score;
    }

    //State evaluation.
    public double value(StateObservationMulti s) {

        boolean gameOver = s.isGameOver();
        double score = s.getGameScore(Game.pId);
        Types.WINNER win = s.getMultiGameWinner()[Game.pId];

        if (gameOver && win == Types.WINNER.PLAYER_LOSES) {
            score += HUGE_NEGATIVE;
        }

        if (gameOver && win == Types.WINNER.PLAYER_WINS) {
            score += HUGE_POSITIVE;
        }

        return score;
    }


    // Action selection.

    public Types.ACTIONS selectAction(Node root) {
        // Ties between equally visited nodes are broken by their value.
        // A random selection is made if nodes have the same number of visits and same value.

        int selected = 0;
        int maxVisits = 0;
        double maxValue = -Double.MAX_VALUE;

        for (int i = 0; i < root.children.length; i++) {
            Node child = root.children[i];
            if (child != null) {
                if (child.visits > maxVisits || (child.visits == maxVisits && child.value > maxValue)
                        || child.visits == maxVisits && child.value == maxValue && Game.random.nextBoolean()) {
//                if (child.value > maxValue || (child.value == maxValue && child.visits > maxVisits)
//                        || child.value == maxValue && child.visits == maxVisits && Game.random.nextBoolean()) {
                    selected = i;
                    maxVisits = child.visits;
                    maxValue = child.value;
                }
            }
        }

        return root.branches[selected];
    }

    public Types.ACTIONS mostVisitedAction(Node root) {

        int selected = 0;
        double max = Double.MIN_VALUE;

        for (int i = 0; i < root.children.length; i++) {
            if (root.children[i] != null) {
                if (root.children[i].visits > max) {
                    selected = i;
                    max = root.children[i].visits;
                }
            }
        }

        return root.branches[selected];
    }

    public Types.ACTIONS mostValuedAction(Node root) {

        int selected = 0;
        double max = Double.MIN_VALUE;

        for (int i = 0; i < root.children.length; i++) {
            if (root.children[i] != null) {
                if (root.children[i].value > max) {
                    selected = i;
                    max = root.children[i].value;
                }
            }
        }

        return root.branches[selected];
    }

    // Actions.

    public Types.ACTIONS[] actions(StateObservationMulti s, int playerId) {
        ArrayList<Types.ACTIONS> a = s.getAvailableActions(playerId);
        return a.toArray(new Types.ACTIONS[a.size()]);
    }

    public Types.ACTIONS randomAction(StateObservationMulti s, int playerId) {
        ArrayList<Types.ACTIONS> a = s.getAvailableActions(playerId);
        return a.get(Game.random.nextInt(a.size()));
    }

    public Types.ACTIONS action(StateObservationMulti s, int playerId, int actionIndex) {
        return s.getAvailableActions(playerId).get(actionIndex);
    }

    // Helper functions.

    private boolean timeRemaining(long tRem, double tAvg) {
        return tRem > 2 * tAvg && tRem > TIME_REMAINING_LIMIT;
    }

    private boolean stop(StateObservationMulti s, int d) {
        return s.isGameOver() || d >= MAX_ROLL_OUT_DEPTH;
    }
}