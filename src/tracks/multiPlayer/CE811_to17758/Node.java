package tracks.multiPlayer.CE811_to17758;

import core.game.StateObservationMulti;
import ontology.Types;

public class Node {

    public int depth;
    public int visits = 0;
    public double value = 0;

    public double upperBound = -Double.MAX_VALUE;
    public double lowerBound = Double.MAX_VALUE;

    public Node parent;
    public Node[] children;
    public Types.ACTIONS[] branches;

    private boolean expanded = false;

    public Node(Node parent) {

        this.parent = parent;
        this.depth = (parent == null) ? 0 : parent.depth + 1;
        this.children = new Node[Game.maxActions];
        this.branches = new Types.ACTIONS[Game.maxActions];
    }

    public boolean expanded(StateObservationMulti s) {
        boolean found = true;
        if (!this.expanded) {
            for (Types.ACTIONS a : s.getAvailableActions(Game.pId)) {
                if (!found) {
                    break;
                }
                found = false;
                for (Types.ACTIONS b : branches) {
                    if (a == b) {
                        found = true;
                        break;
                    }
                }
            }
        }

        return this.expanded = found;
    }

    public void updateBounds(double delta) {
        if (delta > this.upperBound) {
            this.upperBound = delta;
        }

        if (delta < this.lowerBound) {
            this.lowerBound = delta;
        }
    }
}