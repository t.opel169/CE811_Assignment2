package tracks.multiPlayer.CE811_to17758;

import core.game.Observation;
import core.game.StateObservationMulti;
import tools.Vector2d;

import java.util.ArrayList;
import java.util.List;

public class Grid {
    public List<Observation> grid[][];
    public int gridLengthX, gridLengthY;
    public StateObservationMulti state;
    public List<Integer> nontraversableIds;

    public Grid(StateObservationMulti state) {
        this.grid = state.getObservationGrid();
        this.state = state;

        gridLengthX = grid.length;
        gridLengthY = grid[0].length;

        nontraversableIds = new ArrayList<>();
        nontraversableIds.add(0); //wall object, there might be others, but finding them is hard
    }


    public List<GridNode> getNeighbours(GridNode node) {
        List<GridNode> neighbours = new ArrayList<>();

        //TODO TO: Consider games with only 2 actions, but it is bit of a hack, might not work on new games
        for (int i = -1; i <= 1; i++) {
            if (i == 0) continue;

            addNeighbour(node, 0, i, neighbours);
            addNeighbour(node, i, 0, neighbours);
        }

        return neighbours;
    }

    private void addNeighbour(GridNode node, int xPosition, int yPosition, List<GridNode> neighbours) {
        int neighbourX = node.xPosition + xPosition;
        int neighbourY = node.yPosition + yPosition;

        if (neighbourX >= 0 && neighbourX < gridLengthX && neighbourY >= 0 && neighbourY < gridLengthY) {
            GridNode newNode = new GridNode(new Vector2d(neighbourX, neighbourY));
            newNode.isTraversable = isNodeTraversable(newNode);
            neighbours.add(newNode);
        }
    }

    private boolean isNodeTraversable(GridNode node) {
        for (Observation obs : grid[node.xPosition][node.yPosition]) {
            return !nontraversableIds.contains(obs.itype);
        }
        return true;
    }
}