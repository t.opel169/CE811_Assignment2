package tracks.multiPlayer.CE811_to17758;

import java.math.BigDecimal;

public class PlayerStats {
    public String playerName;
    public int numberOfRuns;
    public double minScore = 10000000.0;
    public double maxScore = -10000000.0;
    public double minWinPercentage = 10000000.0;
    public double maxWinPercentage = -10000000.0;
    public double avgWinPercentage;
    public double avgScore;
    public double totalWinPercentage;
    public double totalScore;
    public double totalWinSq;
    public double totalScoreSq;

    public PlayerStats(String playerName){
        this.playerName = playerName;
    }

    public double getStandardErrorWin(){
        double num = totalWinSq - (numberOfRuns * avgWinPercentage * avgWinPercentage);
        if (num < 0) {
            // avoids tiny negative numbers possible through imprecision
            num = 0;
        }
        // System.out.println("Num = " + num);
        double sd = Math.sqrt(num / (numberOfRuns - 1));
        // System.out.println(" tracks.singlePlayer.Test: sd = " + sd);
        // System.out.println(" tracks.singlePlayer.Test: n = " + n);
        return sd;
    }

    public double getStandardErrorScore(){
        double num = totalScoreSq - (numberOfRuns * avgScore * avgScore);
        if (num < 0) {
            // avoids tiny negative numbers possible through imprecision
            num = 0;
        }
        // System.out.println("Num = " + num);
        double sd = Math.sqrt(num / (numberOfRuns - 1));
        // System.out.println(" tracks.singlePlayer.Test: sd = " + sd);
        // System.out.println(" tracks.singlePlayer.Test: n = " + n);
        return sd / Math.sqrt(numberOfRuns);
    }

    public void addGameStats(double winPercentage, double score){
        numberOfRuns++;
        totalWinPercentage += winPercentage;
        totalScore += score;
        totalWinSq += winPercentage * winPercentage;
        totalScoreSq += score * score;
        minScore = minScore > score ? score : minScore;
        maxScore = maxScore < score ? score : maxScore;
        minWinPercentage = minWinPercentage > winPercentage ? winPercentage : minWinPercentage;
        maxWinPercentage = maxWinPercentage < winPercentage ? winPercentage : maxWinPercentage;
        avgWinPercentage = totalWinPercentage / numberOfRuns;
        avgScore = totalScore / numberOfRuns;
    }

    public double getPlusMinusScore(){
        return maxScore - minScore;
    }

    public double getPlusMinusWin(){
        return maxWinPercentage - minWinPercentage;
    }
}
