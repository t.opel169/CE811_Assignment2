package tracks.multiPlayer.CE811_to17758;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class Stats {
    public String gameName;
    public List<PlayerStats> playerStats;

    public Stats(String gameName, String firstPlayerName, String secondPlayerName){
        this.gameName = gameName;
        playerStats = new ArrayList<>();
        playerStats.add(new PlayerStats(firstPlayerName));
        playerStats.add(new PlayerStats(secondPlayerName));
    }

    public void addGameStats(int playerIndex, double winPercentage, double score){
        playerStats.get(playerIndex).addGameStats(winPercentage, score);
    }

    public void calculateGameStats(List<Double> results, List<Double> swappedResults){
        double player1Win, player2Win, player1Score, player2Score;
        player1Win = (results.get(0) + swappedResults.get(2)) / 2;
        player2Win = (results.get(2) + swappedResults.get(0)) / 2;
        player1Score = (results.get(1) + swappedResults.get(3)) / 2;
        player2Score = (results.get(3) + swappedResults.get(1)) / 2;

        addGameStats(0, player1Win, player1Score);
        addGameStats(1, player2Win, player2Score);
    }

    @Override
    public String toString() {
        String stats = "";
        DecimalFormat decFormatTwoDigit = new DecimalFormat("#.##");
        DecimalFormat decFormatOneDigit = new DecimalFormat("#.#");
        decFormatTwoDigit.setRoundingMode(RoundingMode.CEILING);
        decFormatOneDigit.setRoundingMode(RoundingMode.CEILING);
        for (PlayerStats playStats : playerStats){
            stats += playStats.playerName + " avg wins: " + decFormatOneDigit.format((playStats.avgWinPercentage * 100))
                    + " +-" + decFormatOneDigit.format((playStats.getStandardErrorWin() * 100))
                    + " avg score: "
                    + decFormatTwoDigit.format((playStats.avgScore))
                    + " +-" + decFormatTwoDigit.format((playStats.getStandardErrorScore()))
                    + " over " + playStats.numberOfRuns + " runs" + System.lineSeparator();
        }
        return stats;
    }
}
