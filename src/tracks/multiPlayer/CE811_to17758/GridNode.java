package tracks.multiPlayer.CE811_to17758;

import tools.Vector2d;

public class GridNode implements Comparable<GridNode> {
    public GridNode parent;
    public int gCost; //distance from starting node
    public int hCost; //distance from end node
    public Vector2d position;
    public boolean isTraversable = true;
    public int xPosition;
    public int yPosition;

    public GridNode(Vector2d position) {
        this.position = position;
        xPosition = (int) position.x;
        yPosition = (int) position.y;
    }

    @Override
    public boolean equals(Object obj) {
        return position.equals(((GridNode) obj).position);
    }

    @Override
    //comparator for priority queue
    public int compareTo(GridNode node) {
        if (getFCost() < node.getFCost() || (getFCost() == node.getFCost() && hCost < node.hCost)) {
            return -1;
        } else {
            return 1;
        }
    }

    public int getFCost() {
        return gCost + hCost;
    }
}
