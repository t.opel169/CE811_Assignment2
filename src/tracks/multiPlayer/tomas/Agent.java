package tracks.multiPlayer.tomas;

import core.game.StateObservationMulti;
import core.player.AbstractMultiPlayer;
import ontology.Types.ACTIONS;
import tools.ElapsedCpuTimer;

import java.util.Random;

public class Agent extends AbstractMultiPlayer {

    private MonteCarloTreeSearch mcts;

    public Agent(StateObservationMulti s, ElapsedCpuTimer e, int id){

        // J.Madge 01.12.2017 Use ElapsedCpuTimer to ensure don't go past 1 second budget.

        Game2p.playerId = id;
        Game2p.opponentId = (id + 1) % s.getNoPlayers();
        Game2p.random = new Random(System.currentTimeMillis());

        mcts = new MonteCarloTreeSearch();
    }

    @Override
    public ACTIONS act(StateObservationMulti s, ElapsedCpuTimer e) {
        return this.mcts.search(s, e);
    }
}