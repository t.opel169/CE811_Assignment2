package tracks.multiPlayer.tomas;

import ontology.Types;

public class Node {

    private int depth;

    private int visits = 0;
    private double value = 0;
    private double[] bounds = new double[]{Double.MAX_VALUE, -Double.MAX_VALUE};

    private Node parent;
    private Node[] children;

    private Types.ACTIONS[] branches;

    public Node(Node parent, int numActions) {

        this.parent = parent;
        this.depth = (parent == null) ? 0 : parent.depth + 1;
        this.children = new Node[numActions];
        this.branches = new Types.ACTIONS[numActions];
    }

    public int depth()
    {
        return this.depth;
    }

    public int visits()
    {
        return this.visits;
    }

    public double value()
    {
        return this.value;
    }

    public boolean expanded()
    {
        for (Node n : children) {
            if (n == null) {
                return false;
            }
        }

        return true;
    }

    public Node[] children()
    {
        return this.children;
    }

    public void addChild(int idx, Node n)
    {
        this.children[idx] = n;
    }

    public boolean branched(Types.ACTIONS action)
    {
        for (Types.ACTIONS a : branches) {
            if (a == action) {
                return true;
            }
        }

        return false;
    }

    public Types.ACTIONS[] branches()
    {
        return this.branches;
    }

    public void addBranch(int idx, Types.ACTIONS action)
    {
        this.branches[idx] = action;
    }

    public void incrementVisits()
    {
        this.visits++;
    }

    public void updateValue(double delta)
    {
        this.value += delta;
    }

    public Node parent()
    {
        return this.parent;
    }

    public void updateBounds(double delta)
    {
        if (delta < this.bounds[0]) {
            this.bounds[0] = delta;
        }
        if (delta > this.bounds[1]) {
            this.bounds[1] = delta;
        }
    }

    public double upperBound()
    {
        return this.bounds[0];
    }

    public double lowerBound()
    {
        return this.bounds[1];
    }
}