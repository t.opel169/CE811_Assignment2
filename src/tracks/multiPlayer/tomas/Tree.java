package tracks.multiPlayer.tomas;

public class Tree {

    private Node root;

    public Tree(Node r)
    {
        this.root = r;
    }

    public Node root()
    {
        return this.root;
    }
}