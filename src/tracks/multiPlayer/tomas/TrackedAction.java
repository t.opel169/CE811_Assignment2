package tracks.multiPlayer.tomas;

import ontology.Types;

public class TrackedAction {

    public Types.ACTIONS action;
    public boolean taken;

    public TrackedAction(Types.ACTIONS a, boolean b)
    {
        action = a;
        taken = b;
    }
}