package tracks.multiPlayer.tomas;

import core.game.StateObservation;
import core.game.StateObservationMulti;
import ontology.Types;
import tools.ElapsedCpuTimer;
import tools.Utils;

import java.util.ArrayList;

public class MonteCarloTreeSearch {

    private static final double HUGE_POSITIVE =  10000000.0;
    private static final double HUGE_NEGATIVE = -10000000.0;
    private static final double EXPLORATION_CONSTANT = Math.sqrt(2);
    private static final int MAX_ROLLOUT_DEPTH = 10;
    private static final int TIME_REMAINING_LIMIT = 5;

    public MonteCarloTreeSearch(){}

    public Types.ACTIONS search(StateObservationMulti s, ElapsedCpuTimer e)
    {
        double tAvg = 0;    // Average time taken.
        double tAcm = 0;    // Accumulated time taken.
        long tRem = e.remainingTimeMillis();    // Time remaining.
        int i = 0;          // Iterations completed.

        double delta;

        Tree tree = new Tree(new Node(null, numActions(s, Game2p.playerId)));
        Node n;

        StateObservationMulti si;
        ElapsedCpuTimer elps;

        breadthFirst(tree.root(), s);
        while(timeRemaining(tRem, tAvg)){
        //while(i < 100 ){
            si = s.copy();
            elps = new ElapsedCpuTimer();

            n = treePolicy(tree.root(), si);
            delta = rollout(n, si);
            backpropagation(n, delta);

            tAcm += (elps.elapsedMillis());
            tAvg  = tAcm/++i;
            tRem = e.remainingTimeMillis();
        }

        //System.out.println(i);
        //System.out.println(mostVisitedAction(tree.root()));
        return mostVisitedAction(tree.root());
    }

    private void breadthFirst(Node rootNode, StateObservationMulti state){
        int extraIterations = 1;
        int actionIndex = 0;
        Types.ACTIONS[] playerActions = actions(state, Game2p.playerId);
        //expand all nodes Breadth-First
        while(!rootNode.expanded()){
            StateObservationMulti stateCopy = state.copy();
            Node expandedNode = expand(rootNode, stateCopy, actionIndex);

            Types.ACTIONS playerAction;
            playerAction = playerActions[actionIndex];
            rootNode.addBranch(actionIndex, playerAction);

            double scoreTotal = value(stateCopy);
            //do more iterations because of nondeterminism
            for (int i = 0; i < extraIterations; i++){
                StateObservationMulti copiedState = state.copy();
                advanceState(copiedState, actionIndex);
                scoreTotal += value(copiedState);
            }
            backpropagation(expandedNode, scoreTotal / (extraIterations + 1));
            actionIndex++;
        }
        //System.out.println("Breadth-first done with " + extraIterations + " extra iterations");
    }

    private Node treePolicy(Node root, StateObservationMulti s)
    {
        Node n = root;

        while(!stop(s, n.depth()))
        {
            if(!n.expanded())
            {
                int actionToExpand = randomNovelPlayerActionId(n, s);
                return expand(n, s, actionToExpand);
            }
            else
            {
                n = utc(n, s);
            }
        }

        return n;
    }

    private double rollout(Node n, StateObservationMulti s)
    {
        double delta;
        int d = n.depth();

        while (!stop(s, d)) {

            Types.ACTIONS[] actions = new Types.ACTIONS[Game2p.players];
            actions[Game2p.playerId] = actions(s, Game2p.playerId)[randomPlayerActionId(s)];
            actions[Game2p.opponentId] = actions(s, Game2p.opponentId)[randomOpponentActionId(s)];

            s.advance(actions);
            d++;
        }

        delta = value(s);
        n.updateBounds(delta);

        return delta;
    }

    private void backpropagation(Node n, double delta)
    {
        while(n != null)
        {
            n.incrementVisits();
            n.updateValue(delta);
            n.updateBounds(delta);
            n = n.parent();
        }
    }

    public double value(StateObservationMulti s) {

        boolean gameOver = s.isGameOver();

        Types.WINNER win = s.getMultiGameWinner()[Game2p.playerId];

        double score = s.getGameScore(Game2p.playerId);

        if(gameOver && win == Types.WINNER.PLAYER_LOSES){
            score += HUGE_NEGATIVE;
        }

        if(gameOver && win == Types.WINNER.PLAYER_WINS){
            score += HUGE_POSITIVE;
        }

        return score;
    }

    private Node utc(Node n, StateObservationMulti s)
    {
        double utc;
        double max = -Double.MAX_VALUE;
        int selectedIdx = 0;
        Node selected = null;
        Node[] children = n.children();
        for (int i = 0; i < children.length; i++) {
            if (children[i] != null)
            {
                utc = Utils.normalise(children[i].value()/children[i].visits(), n.upperBound(), n.lowerBound())
                        + (EXPLORATION_CONSTANT * Math.sqrt((2 * Math.log(n.visits())) / (children[i].visits())));

                if (utc > max)
                {
                    selected = children[i];
                    selectedIdx = i;
                    max = utc;
                }
            }
        }

        Types.ACTIONS[] actions = new Types.ACTIONS[Game2p.players];
        actions[Game2p.playerId] = n.branches()[selectedIdx];
        actions[Game2p.opponentId] = actions(s, Game2p.opponentId)[randomOpponentActionId(s)];

        s.advance(actions);

        return selected;
    }

    private Node expand(Node n, StateObservationMulti s, int actionIndex)
    {
        advanceState(s, actionIndex);

        Node c = new Node(n, numActions(s, Game2p.playerId));
        n.addChild(actionIndex, c);

        return c;
    }
    
    private void advanceState(StateObservationMulti state, int actionIndex){
        Types.ACTIONS[] actions = new Types.ACTIONS[Game2p.players];
        actions[Game2p.playerId] = actions(state, Game2p.playerId)[actionIndex];
        actions[Game2p.opponentId] = actions(state, Game2p.opponentId)[randomOpponentActionId(state)];

        state.advance(actions);
    }

    private boolean timeRemaining(long tRem, double tAvg)
    {
        return tRem > 2*tAvg && tRem > TIME_REMAINING_LIMIT;
    }

    private int randomNovelPlayerActionId(Node n, StateObservationMulti s)
    {
        int actionId;
        Types.ACTIONS playerAction;
        Types.ACTIONS[] playerActions = actions(s, Game2p.playerId);

        do {
            actionId = Game2p.random.nextInt(playerActions.length);
            playerAction = playerActions[actionId];
        } while (n.branched(playerAction));

        n.addBranch(actionId, playerAction);

        return actionId;
    }

    private int randomOpponentActionId(StateObservationMulti s)
    {
        return Game2p.random.nextInt(numActions(s, Game2p.opponentId));
    }

    private int randomPlayerActionId(StateObservationMulti s)
    {
        return Game2p.random.nextInt(numActions(s, Game2p.playerId));
    }

    public Types.ACTIONS[] actions(StateObservationMulti s, int id)
    {
        ArrayList<Types.ACTIONS> a =  s.getAvailableActions(id);
        return a.toArray(new Types.ACTIONS[a.size()]);
    }

    public int numActions(StateObservationMulti s, int id)
    {
        return s.getAvailableActions(id).size();
    }

    public boolean stop(StateObservationMulti s, int d)
    {
        return s.isGameOver() || d >= MAX_ROLLOUT_DEPTH;
    }

    public Types.ACTIONS mostVisitedAction(Node root) {

        int selected = 0;
        double max = -Double.MAX_VALUE;

        Node[] children = root.children();
        for (int i=0; i< children.length; i++) {
            if(children[i] != null) {
                if (children[i].visits() > max) {
                    selected = i;
                    max = children[i].visits();
                }
            }
        }

        return root.branches()[selected];
    }
}