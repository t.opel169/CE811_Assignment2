package tracks.multiPlayer.tomas;

public interface IPolicy {
    Node select(Node[] n);
}