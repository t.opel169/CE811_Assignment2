package tracks.multiPlayer.tomas;

import ontology.Types;

public interface IActionSelector {

    Types.ACTIONS SelectPlayerAction(Types.ACTIONS[] available, Types.ACTIONS[] taken);
    Types.ACTIONS SelectOpponentAction(Types.ACTIONS[] available, Types.ACTIONS[] taken);
}