package tracks.multiPlayer.james;

import core.game.StateObservationMulti;
import ontology.Types;
import tools.ElapsedCpuTimer;
import tools.Utils;

import java.util.ArrayList;

public class MonteCarloTreeSearch {

    private Kbe kbe;

    private static final double HUGE_POSITIVE = 10000000.0;
    private static final double HUGE_NEGATIVE = -10000000.0;
    private static final double EXPLORATION_CONSTANT = Math.sqrt(2); // Higher value -> More exploration.
    private static final int MAX_ROLLOUT_DEPTH = 10;
    private static final int TIME_REMAINING_LIMIT = 5;

    public MonteCarloTreeSearch(StateObservationMulti s) {
        this.kbe = new Kbe(s);
    }

    // MCTS core functions.

    public Types.ACTIONS search(StateObservationMulti s, ElapsedCpuTimer e) {
        double tAvg = 0;                        // Average time taken.
        double tAcm = 0;                        // Accumulated time taken.
        long tRem = e.remainingTimeMillis();    // Time remaining.
        int i = 0;                              // Iterations completed.

        double delta;

        Node n;
        Node root = new Node(null, numActions(s, Game.pId));

        StateObservationMulti si;
        ElapsedCpuTimer elps;

        kbe.incrementWeights();

        while (timeRemaining(tRem, tAvg)) {
            si = s.copy();
            elps = new ElapsedCpuTimer();

            n = treePolicy(root, si);
            delta = rollout(n, si);
            backpropagation(n, delta);

            tAcm += (elps.elapsedMillis());
            tAvg = tAcm / ++i;
            tRem = e.remainingTimeMillis();
        }

        return mostVisitedAction(root);
    }

    private Node treePolicy(Node root, StateObservationMulti s) {
        Node n = root;

        while (!stop(s, n.depth)) {
            if (!n.expanded()) {
                return expand(n, s);
            } else {
                n = utc(n, s);
            }
        }

        return n;
    }

    private double rollout(Node n, StateObservationMulti s) {
        double value;
        double valuePrior;

        int d = n.depth;

        valuePrior = value(s);
        kbe.observe(s); // Store IDs of all observed sprites.
        kbe.updateSpriteDistancesPrior(s); // Compute and store distances at s0 (current game state).

        Types.ACTIONS[] actions = new Types.ACTIONS[Game.players];
        while (!stop(s, d)) {
            actions[Game.pId] = actions(s, Game.pId)[randomPlayerActionId(s)];
            actions[Game.oId] = actions(s, Game.oId)[randomOpponentActionId(s)];

            s.advance(actions);
            d++;
        }

        value = value(s);

        if (value == valuePrior) {
            kbe.updateSpriteDistances(s);  // Compute and store distances at sT (final state of play-out).
            value += kbe.evaluate();        // Add EVAL KB(sT) term to value.
        } else { // Some collision assumed to cause score change.
            if (!s.getEventsHistory().isEmpty()) {
                kbe.updateWeight(s.getEventsHistory().last().passiveTypeId, value);
            }
        }

        return value;
    }

    private void backpropagation(Node n, double delta) {
        while (n != null) {
            n.visits++;
            n.value += delta;
            n.updateBounds(delta);
            n = n.parent;
        }
    }

    private Node utc(Node n, StateObservationMulti s) {
        double utc;
        double max = -Double.MAX_VALUE;
        int selectedIdx = 0;
        Node selected = null;
        for (int i = 0; i < n.children.length; i++) {
            if (n.children[i] != null) {

                utc = Utils.normalise((n.children[i].value / n.children[i].visits), n.lowerBound, n.upperBound)
                        + (EXPLORATION_CONSTANT * (Math.sqrt((Math.log(n.visits)) / n.children[i].visits)));

                if (utc > max) {
                    selected = n.children[i];
                    selectedIdx = i;
                    max = utc;
                }
            }
        }

        Types.ACTIONS[] actions = new Types.ACTIONS[Game.players];
        actions[Game.pId] = n.branches[selectedIdx];
        actions[Game.oId] = actions(s, Game.oId)[randomOpponentActionId(s)];

        s.advance(actions);

        return selected;
    }

    private Node expand(Node n, StateObservationMulti s) {
        int i = randomNovelPlayerActionId(n, s);

        Types.ACTIONS[] actions = new Types.ACTIONS[Game.players];
        actions[Game.pId] = actions(s, Game.pId)[i];
        actions[Game.oId] = actions(s, Game.oId)[randomOpponentActionId(s)];

        n.children[i] = new Node(n, numActions(s, Game.pId));

        s.advance(actions);

        return n.children[i];
    }

    // State evaluation.

    public double value(StateObservationMulti s) {

        boolean gameOver = s.isGameOver();
        double score = s.getGameScore(Game.pId);
        Types.WINNER win = s.getMultiGameWinner()[Game.pId];

        if (gameOver && win == Types.WINNER.PLAYER_LOSES) {
            score += HUGE_NEGATIVE;
        }

        if (gameOver && win == Types.WINNER.PLAYER_WINS) {
            score += HUGE_POSITIVE;
        }

        return score;
    }

    // Action selection.

    public Types.ACTIONS mostVisitedAction(Node root) {

        // TODO J.Madge What if there is a tie?
        // TODO Should the tie be broken randomly?
        // TODO Should it be broken by highest value?
        int selected = 0;
        double max = -Double.MAX_VALUE;

        for (int i = 0; i < root.children.length; i++) {
            if (root.children[i] != null) {
                if (root.children[i].visits > max) {
                    selected = i;
                    max = root.children[i].visits;
                }
            }
        }

        return root.branches[selected];
    }

    public Types.ACTIONS mostValuedAction(Node root) {

        // TODO J.Madge What if there is a tie?
        int selected = 0;
        double max = -Double.MAX_VALUE;
        for (int i = 0; i < root.children.length; i++) {
            if (root.children[i] != null) {
                if (root.children[i].value > max) {
                    selected = i;
                    max = root.children[i].value;
                }
            }
        }

        return root.branches[selected];
    }

    // Actions.

    public Types.ACTIONS[] actions(StateObservationMulti s, int id) {
        ArrayList<Types.ACTIONS> a = s.getAvailableActions(id);
        return a.toArray(new Types.ACTIONS[a.size()]);
    }

    private int numActions(StateObservationMulti s, int id) {
        return s.getAvailableActions(id).size();
    }

    private int randomOpponentActionId(StateObservationMulti s) {
        return Game.random.nextInt(numActions(s, Game.oId));
    }

    private int randomPlayerActionId(StateObservationMulti s) {
        return Game.random.nextInt(numActions(s, Game.pId));
    }

    private int randomNovelPlayerActionId(Node n, StateObservationMulti s) {
        int actionId;
        Types.ACTIONS playerAction;
        Types.ACTIONS[] playerActions = actions(s, Game.pId);

        do {
            actionId = Game.random.nextInt(playerActions.length);
            playerAction = playerActions[actionId];
        } while (n.branched(playerAction)); // TODO J.Madge Efficiency saving here?

        n.branches[actionId] = playerAction;

        return actionId;
    }

    // Helper functions.

    private boolean timeRemaining(long tRem, double tAvg) {
        return tRem > 2 * tAvg && tRem > TIME_REMAINING_LIMIT;
    }

    private boolean stop(StateObservationMulti s, int d) {
        return s.isGameOver() || d >= MAX_ROLLOUT_DEPTH;
    }
}