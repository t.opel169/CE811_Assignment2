package tracks.multiPlayer.james;

import java.util.Random;

public class Game {

    public static int pId;          // ID of the player.
    public static int oId;          // ID of the opponent.
    public static int players = 2;  // Number of players in the game.
    public static Random random;    // Random number generator used during the game.
}