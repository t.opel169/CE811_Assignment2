package tracks.multiPlayer.james;

import tools.Vector2d;

public class Euclidean {

    public static double twoDimensions(Vector2d pos1, Vector2d pos2) {
        return Math.sqrt(Math.pow((pos1.x - pos2.x), 2) + Math.pow((pos1.y - pos2.y), 2));
    }

    public static double oneDimension(int height, int width) {
        return Math.sqrt(Math.pow((height), 2) + Math.pow((width), 2));
    }
}