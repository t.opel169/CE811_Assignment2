package tracks.multiPlayer.james;

import ontology.Types;

public class Node {

    public int depth;
    public int visits = 0;
    public double value = 0;

    public double upperBound = -Double.MAX_VALUE;
    public double lowerBound = Double.MAX_VALUE;

    public Node[] children;

    public Node parent;
    public Types.ACTIONS[] branches;

    private boolean expanded = false;

    public Node(Node parent, int numActions) {

        this.parent = parent;
        this.depth = (parent == null) ? 0 : parent.depth + 1;
        this.children = new Node[numActions];
        this.branches = new Types.ACTIONS[numActions];
    }

    public boolean expanded() {
        if (!this.expanded) {
            for (Node n : children) {
                if (n == null) {
                    return false;
                }
            }
        }

        return this.expanded = true;
    }

    public boolean branched(Types.ACTIONS action) {
        // TODO Efficiency saving here? Dictionary/map?
        for (Types.ACTIONS a : branches) {
            if (a == action) {
                return true;
            }
        }

        return false;
    }

    public void updateBounds(double delta) {
        if (delta > this.upperBound) {
            this.upperBound = delta;
        }

        if (delta < this.lowerBound) {
            this.lowerBound = delta;
        }
    }
}