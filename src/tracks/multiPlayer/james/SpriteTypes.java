package tracks.multiPlayer.james;

public enum SpriteTypes {
    MOVABLE,
    NPC,
    PORTAL,
    RESOURCE
}