package tracks.multiPlayer.james;

import core.game.StateObservationMulti;
import core.player.AbstractMultiPlayer;
import ontology.Types.ACTIONS;
import tools.ElapsedCpuTimer;

import java.util.Random;

public class Agent extends AbstractMultiPlayer {

    private MonteCarloTreeSearch mcts;

    public Agent(StateObservationMulti s, ElapsedCpuTimer e, int id) {

        Game.pId = id;
        Game.oId = (id + 1) % s.getNoPlayers();
        Game.random = new Random(System.currentTimeMillis());

        mcts = new MonteCarloTreeSearch(s);
    }

    @Override
    public ACTIONS act(StateObservationMulti s, ElapsedCpuTimer e) {
        return this.mcts.search(s, e);
    }
}