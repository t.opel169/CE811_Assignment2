package tracks.multiPlayer.james;

import core.game.Observation;
import core.game.StateObservationMulti;
import tools.Utils;
import tools.Vector2d;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Kbe {

    // Initial weights.
    private static final double WEIGHT_INCREMENT = 0.0001;
    private static final double WEIGHT_INITIAL_MOVABLE = 0.1;
    private static final double WEIGHT_INITIAL_NPC = 0.25;
    private static final double WEIGHT_INITIAL_PORTAL = 1.0;
    private static final double WEIGHT_INITIAL_RESOURCE = 1.0;
    private double[] spriteWeights = new double[SpriteTypes.values().length];

    // Bounds (for normalisation).
    private final double KBE_MIN;
    private final double KBE_MAX;

    // Distances.
    private double[] spriteDist = new double[SpriteTypes.values().length];
    private double[] spriteDistPrior = new double[SpriteTypes.values().length];

    // Learning rate.
    private static final double LEARNING_RATE_INITIAL = 0.8;
    private static final double LEARNING_RATE_MIN = 0.1;
    private static final double LEARNING_RATE_GRADIENT = 0.75;
    private double[] learningRate = new double[SpriteTypes.values().length];

    // Observed IDs of each type.
    private Set<Integer> idsResource = new HashSet<>();
    private Set<Integer> idsNpc = new HashSet<>();
    private Set<Integer> idsMovable = new HashSet<>();
    private Set<Integer> idsPortal = new HashSet<>();

    // Collision score change.
    private double[] scoreChangeAccumulation = new double[SpriteTypes.values().length];

    // Collision count.
    private double[] collisionCount = new double[SpriteTypes.values().length];

    public Kbe(StateObservationMulti s) {
        // Weight initialisation.
        spriteWeights[SpriteTypes.MOVABLE.ordinal()] = WEIGHT_INITIAL_MOVABLE;
        spriteWeights[SpriteTypes.NPC.ordinal()] = WEIGHT_INITIAL_NPC;
        spriteWeights[SpriteTypes.PORTAL.ordinal()] = WEIGHT_INITIAL_PORTAL;
        spriteWeights[SpriteTypes.RESOURCE.ordinal()] = WEIGHT_INITIAL_RESOURCE;

        // Learning rate initialization.
        learningRate[SpriteTypes.MOVABLE.ordinal()] = LEARNING_RATE_INITIAL;
        learningRate[SpriteTypes.NPC.ordinal()] = LEARNING_RATE_INITIAL;
        learningRate[SpriteTypes.PORTAL.ordinal()] = LEARNING_RATE_INITIAL;
        learningRate[SpriteTypes.RESOURCE.ordinal()] = LEARNING_RATE_INITIAL;

        // Bounds (for normalisation) initialisation.
        Dimension d = s.getWorldDimension();
        KBE_MAX = Math.pow(d.height, 2) + Math.pow(d.width, 2); // Max distance from sprite.
        KBE_MIN = 0; // Min distance from sprite.
    }

    public double evaluate() {
        double kbe = 0;
        kbe += spriteKbe(SpriteTypes.MOVABLE);
        kbe += spriteKbe(SpriteTypes.NPC);
        kbe += spriteKbe(SpriteTypes.PORTAL);
        kbe += spriteKbe(SpriteTypes.RESOURCE);
        return -Utils.normalise(kbe, KBE_MIN, KBE_MAX) * 0.5; // Between 0 and -0.5.
    }

    public void observe(StateObservationMulti s) {
        idsMovable.addAll(ids(s.getMovablePositions()));
        idsNpc.addAll(ids(s.getNPCPositions()));
        idsPortal.addAll(ids(s.getPortalsPositions()));
        idsResource.addAll(ids(s.getResourcesPositions()));
    }

    public void updateWeight(int id, double delta) {
        SpriteTypes s = null;

        if (idsMovable.contains(id)) {
            s = SpriteTypes.MOVABLE;
        } else if (idsNpc.contains(id)) {
            s = SpriteTypes.NPC;
        } else if (idsPortal.contains(id)) {
            s = SpriteTypes.PORTAL;
        } else if (idsResource.contains(id)) {
            s = SpriteTypes.RESOURCE;
        }

        if (s != null) {
            updateWeight(s, delta);
            updateLearningRate(s);
        }
    }

    public void incrementWeights() {
        spriteWeights[SpriteTypes.MOVABLE.ordinal()] += WEIGHT_INCREMENT;
        spriteWeights[SpriteTypes.NPC.ordinal()] += WEIGHT_INCREMENT;
        spriteWeights[SpriteTypes.PORTAL.ordinal()] += WEIGHT_INCREMENT;
        spriteWeights[SpriteTypes.RESOURCE.ordinal()] += WEIGHT_INCREMENT;
    }

    public void updateSpriteDistances(StateObservationMulti s) {
        this.distance(s, this.spriteDist);
    }

    public void updateSpriteDistancesPrior(StateObservationMulti s) {
        this.distance(s, this.spriteDistPrior);
    }

    private void updateWeight(SpriteTypes s, double delta) {
        spriteWeights[s.ordinal()] = spriteWeights[s.ordinal()] +
                ((scoreChangeAccumulation[s.ordinal()] += delta) / ++collisionCount[s.ordinal()]) * learningRate[s.ordinal()];
    }

    private void updateLearningRate(SpriteTypes s) {
        learningRate[s.ordinal()] = Math.max(LEARNING_RATE_MIN, LEARNING_RATE_GRADIENT * learningRate[s.ordinal()]);
    }

    private ArrayList<Integer> ids(ArrayList<Observation>[] arg) {
        ArrayList<Integer> results = new ArrayList<>();

        if (arg == null) {
            return results;
        }

        for (ArrayList<Observation> arr : arg) {
            for (Observation o : arr) {
                results.add(o.itype);
            }
        }

        return results;
    }

    private double spriteKbe(SpriteTypes s) {
        double kbe = spriteWeights[s.ordinal()] * (spriteDistPrior[s.ordinal()] - spriteDist[s.ordinal()]);
        return kbe < 0.0 ? 0.0 : kbe; // Minimum KBE value is 0.
    }

    private void distance(StateObservationMulti s, double[] d) {
        Vector2d pPos = s.getAvatarPosition(Game.pId);
        d[SpriteTypes.MOVABLE.ordinal()] = spriteDistance(s.getMovablePositions(pPos));
        d[SpriteTypes.NPC.ordinal()] = spriteDistance(s.getNPCPositions(pPos));
        d[SpriteTypes.PORTAL.ordinal()] = spriteDistance(s.getPortalsPositions(pPos));
        d[SpriteTypes.RESOURCE.ordinal()] = spriteDistance(s.getResourcesPositions(pPos));
    }

    private double spriteDistance(ArrayList<Observation>[] arg) {

        boolean found = false;
        double min = Double.MAX_VALUE;

        if (arg == null) {
            return 0.0;
        }

        for (ArrayList<Observation> o : arg) {  // TODO Efficiency saving here?
            if (o.size() > 0) {
                found = true;
            }
        }

        if (!found) {
            return 0.0;
        }

        for (ArrayList<Observation> o : arg) {
            if (o.size() > 0) {
                if (o.get(0).sqDist < min) { // Elements pre-ordered closest to the avatar.
                    min = o.get(0).sqDist;
                }
            }
        }

        return min;
    }
}